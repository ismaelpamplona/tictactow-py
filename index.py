import os
import random


def clear_output():
    os.system('cls')


def display_board(board):
    print("")
    print("Postions: ")
    print("")
    print(" 7 | 8 | 9")
    print("-----------")
    print(" 4 | 5 | 6")
    print("-----------")
    print(" 1 | 2 | 3")
    print("")
    print("Board: ")
    print("")
    print(" " + board[7] + " | " + board[8] + " | " + board[9])
    print("-----------")
    print(" " + board[4] + " | " + board[5] + " | " + board[6])
    print("-----------")
    print(" " + board[1] + " | " + board[2] + " | " + board[3])
    print("")


def player_input():
    marker = ""
    while not (marker == "X" or marker == "O"):
        marker = input("Player 1: Do you wanna play X or O? ").upper()

    if marker == "X":
        return ("X", "O")
    else:
        return ("O", "X")


def place_marker(board, marker, position):
    board[position] = marker


def win_check(board, mark):
    return (
        # top win
        (board[7] == mark and board[8] == mark and board[9] == mark) or
        # horizontal middle win
        (board[4] == mark and board[5] == mark and board[6] == mark) or
        # vertical middle win
        (board[8] == mark and board[5] == mark and board[2] == mark) or
        # bottom win
        (board[1] == mark and board[2] == mark and board[3] == mark) or
        # left win
        (board[7] == mark and board[4] == mark and board[1] == mark) or
        # right win
        (board[9] == mark and board[6] == mark and board[3] == mark) or
        # diagonal win
        (board[1] == mark and board[5] == mark and board[9] == mark) or
        # diagonal win
        (board[7] == mark and board[5] == mark and board[3] == mark)
    )


def choose_first():
    if random.randint(0, 1) == 0:
        return "Player 2"
    else:
        return "Player 1"


def space_check(board, position):
    return board[position] == " "


def full_board_check(board):
    for i in range(0, 10):
        if space_check(board, i):
            return False
    return True


def player_choice(board, player):
    position = " "
    while position not in "1 2 3 4 5 6 7 8 9".split() or not space_check(board, int(position)):
        position = input(player + ", choose your move! (1-9) ")
    return int(position)


def replay():
    return input('Do you wanna play again? "YES" or "NO"? ').lower().startswith("y")


print("Welcome to TIC TAC TOW in Python - The Game!")

while True:
    board = [" "] * 10
    player1_marker, player2_marker = player_input()
    turn = choose_first()
    print(turn + " starts!")
    game_on = True

    while game_on:
        # Player 1 turn
        if turn == "Player 1":
            display_board(board)
            position = player_choice(board, turn)
            place_marker(board, player1_marker, position)

        # Win ckeck
        if win_check(board, player1_marker):
            display_board(board)
            print("Congratulations Player 1! You won the game!")
            game_on = False
        else:
            if full_board_check(board):
                display_board(board)
                print("The game tied!")
                break
            else:
                turn = "Player 2"

        # Player 2 turn
        if turn == "Player 2":
            display_board(board)
            position = player_choice(board, turn)
            place_marker(board, player2_marker, position)

        # Win ckeck
        if win_check(board, player2_marker):
            display_board(board)
            print("Congratulations Player 2! You won the game!")
            game_on = False
        else:
            if full_board_check(board):
                display_board(board)
                print("The game tied!")
                break
            else:
                turn = "Player 1"

    if not replay():
        break
